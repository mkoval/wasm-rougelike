import Game from "./Game";

type PlayerCore = {
    draw(): void;
    x(): number;
    y(): number;
}

export default abstract class Playable {
    protected constructor(private core: PlayerCore) {
        this.core.draw();
    }

    public abstract act(): void;

    public get x(): number {
        return this.core.x();
    }

    public get y(): number {
        return this.core.y();
    }
}

export interface PlayableConstructor {
    new(x: number, y: number, game: Game): Playable;
}
