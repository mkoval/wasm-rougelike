import { Path } from "rot-js";
import Playable from "./Playable";
import Game from "./Game";

export default class Checko extends Playable {
    constructor (x: number, y: number, private game: Game) {
        super(new PlayerCore(x, y, "B", "red", this.game.display));
    }

    public act () {
        let x = this.game.player.x,
            y = this.game.player.y,
            astar = new Path.AStar(x, y, (x, y) => {
                return this.game.engine.free_cell(x, y);
            }, { topology: 4 }),
            path: [number, number][] = [];

        astar.compute(this.core.x(), this.core.y(), (x, y) => {
            path.push([x, y]);
        });

        path.shift();

        if (path.length <= 1) {
            this.game.rotengine.lock();
            alert("Game over!");
        } else {
            x = path[0][0];
            y = path[0][1];
            this.game.engine.move_player(this.core, x, y);
        }
    }
}
