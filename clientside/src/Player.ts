import { DIRS } from 'rot-js';
import Playable from "./Playable";
import Game from "./Game";

export default class Player extends Playable {
    constructor (x: number, y: number, private game: Game) {
        super(new PlayerCore(x, y, "@", "#ff0", this.game.display));
    }

    public act () {
        this.game.rotengine.lock();
        window.addEventListener("keydown", this);
    }

    handleEvent (event: KeyboardEvent) {
        var keyMap = {};
        keyMap[38] = 0;
        keyMap[33] = 1;
        keyMap[39] = 2;
        keyMap[34] = 3;
        keyMap[40] = 4;
        keyMap[35] = 5;
        keyMap[37] = 6;
        keyMap[36] = 7;
    
        var code = event.keyCode;
    
        if (code == 13 || code == 32) {
            this.game.engine.open_box(this.core, this.core.x(), this.core.y());
            return;
        }
    
        /* one of numpad directions? */
        if (!(code in keyMap)) { return; }
    
        /* is there a free space? */
        var dir = DIRS[8][keyMap[code]];
        var newX = this.core.x() + dir[0];
        var newY = this.core.y() + dir[1];
    
        if (!this.game.engine.free_cell(newX, newY)) { return; };
    
        this.game.engine.move_player(this.core, newX, newY);
        window.removeEventListener("keydown", this);
        this.game.rotengine.unlock();
    }
}

