/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_engine_free(a: number): void;
export function engine_new(a: number): number;
export function engine_on_dig(a: number, b: number, c: number, d: number): void;
export function engine_draw_map(a: number): void;
