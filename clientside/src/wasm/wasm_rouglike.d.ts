/* tslint:disable */
/* eslint-disable */
export class Engine {
  free(): void;
/**
* @param {any} display 
*/
  constructor(display: any);
/**
* @param {number} x 
* @param {number} y 
* @param {number} val 
*/
  on_dig(x: number, y: number, val: number): void;
/**
*/
  draw_map(): void;
}
