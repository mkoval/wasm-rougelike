import { Display } from 'rot-js'
import Game from './Game';

async function init() {
    const wasm = await import('./wasm/wasm_rouglike.js');
    const display = new Display({width: 125, height: 40});

    document.getElementById("rogueCanvas")!.appendChild(display.getContainer()!);

    const engine = new wasm.Engine(display);
    
    const game = new Game(engine, display);
}

init();