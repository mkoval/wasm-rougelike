import { Display, RNG, Map, Scheduler, Engine as RotEngine } from 'rot-js';
import { Engine } from './wasm/wasm_rouglike';
import Playable, { PlayableConstructor } from "./Playable";
import Player from './Player';
import Checko from "./Checko";

type Stats = {
    [propName: string]: string;
}

export default class Game {
    display: Display;
    engine: Engine;
    map: string[] = [];
    freeCells: string[] = [];
    statsContainerEl: HTMLElement;
    rotengine: RotEngine;
    player: Playable;
    enemy: Playable;

    constructor(engine: Engine, display: Display) {
        let scheduler = new Scheduler.Simple();

        this.display = display;
        this.engine = engine;

        this.generateMap();

        this.player = this.createBeing(Player);
        this.enemy = this.createBeing(Checko);

        this.rotengine = new RotEngine(scheduler);
        this.rotengine.start();

        this.statsContainerEl = document.querySelector('#statsContainer')! as HTMLElement;
    }

    private generateMap() {
        // @ts-ignore
        let digger = new Map.Digger();

        digger.create(this.digCallback.bind(this));

        this.generateBoxes();
        this.engine.draw_map();
    }

    private generateBoxes() {
        let { freeCells } = this;

        for (let i = 0; i < 10; i++) {
            let index = Math.floor(RNG.getUniform() * freeCells.length);
            let key = freeCells.splice(index, 1)[0];
            let parts = key.split(",");
            let x = parseInt(parts[0]);
            let y = parseInt(parts[1]);
            this.engine.place_box(x, y);

            if (i == 9) {
                this.engine.mark_wasmprize(x, y);
            }
        }
    };

    private updateStats(stats: Stats) {
        for (let [field, value] of Object.entries(stats)) {
            let statField = this.statsContainerEl.querySelector(`#${field}`) as HTMLElement;

            statField!.innerText = stats[field];
        }
    }

    private digCallback(x: number, y: number, value: number) {
        if (!value) {
            let key = `${x},${y}`;
            this.freeCells.push(key);
        }

        this.engine.on_dig(x, y, value);
    }

    private createBeing (what: PlayableConstructor): Playable {
        let {freeCells} = this;
        let index = Math.floor(RNG.getUniform() * freeCells.length);
        let key = freeCells.splice(index, 1)[0];
        let parts = key.split(",");
        let x = parseInt(parts[0]);
        let y = parseInt(parts[1]);
        return new what(x, y, this);
    }


}
